import re

from flask import render_template, flash, redirect, url_for, request
from APP_FILMS import obj_mon_application
from APP_FILMS.RESERVOIR.data_gestion_reservoir import GestionReservoir
from APP_FILMS.DATABASE.erreurs import *


# OM 2020.04.10 Pour utiliser les expressions régulières REGEX

@obj_mon_application.route("/reservoir_afficher", methods=['GET', 'POST'])
def reservoir_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_films = GestionReservoir()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionFilms()
            # Fichier data_gestion_films.py
            data_films = obj_actions_films.reservoir_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data films", data_films, "type ", type(data_films))
            # Différencier les messages si la table est vide.
            if data_films:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données films affichées !!", "success")
            else:
                flash("""La table "t_films" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}", "danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("reservoir/reservoir_afficher.html", data=data_films)
