/*
	Affiche Toutes les personnes et leur vehicule
*/
SELECT * FROM t_personnes_vehicules AS T1
INNER JOIN t_personnes AS T2 ON T2.id_Personne = T1.FK_Personne
INNER JOIN t_vehicules AS T3 ON T3.id_Vehicule = T1.FK_Vehicule

/*
	Affiche tous les vehicules avec leur pleins
*/
SELECT * FROM t_vehicules_reservoir AS T1
INNER JOIN t_vehicules AS T2 ON T2.id_Vehicule = T1.FK_Vehicule
INNER JOIN t_reservoir AS T3 ON T3.id_Reservoir = T1.FK_Reservoir

/*
	Affiche tous les vehicules avec leur frais
*/
SELECT * FROM t_vehicules_frais AS T1
INNER JOIN t_vehicules AS T2 ON T2.id_Vehicule = T1.FK_Vehicule
INNER JOIN t_frais AS T3 ON T3.id_Frais = T1.FK_Frais

/*
	Seulement certaines colonnes (Le nom et prénom de la t_personnes puis la marque et le modèle du t_vehicules)
*/
SELECT NomPersonne , PrenomPersonne, MarqueVehicule, ModeleVehicule FROM t_personnes_vehicules AS T1
INNER JOIN t_personnes AS T2 ON T2.id_Personne = T1.fk_Personne
INNER JOIN t_vehicules AS T3 ON T3.id_Vehicule = T1.fk_Vehicule

/*
	Permet d'afficher toutes les lignes de la table de droite (t_personnes) (qui est écrite en sql à droite de t_personnes_vehicules)
	y compris les lignes qui ne sont pas attribuées à des vehicules.
*/
SELECT id_Personne, PrenomPersonne, NomPersonne, MarqueVehicule, ModeleVehicule FROM t_personnes_vehicules AS T1
INNER JOIN t_vehicules AS T2 ON T2.id_Vehicule = T1.fk_Vehicule
RIGHT JOIN t_personnes AS T3 ON T3.id_Personne = T1.fk_Personne

/*
	Permet d'afficher toutes les lignes de la table de droite (t_personnes) (qui est écrite en sql à droite de t_personnes_mails)
	y compris les lignes qui ne sont pas attribuées à des mails.
*/
SELECT id_Personne, PrenomPersonne, NomPersonne, id_Mail, NomMail FROM t_personnes_mails AS T1
INNER JOIN t_mails AS T2 ON T2.id_Mail = T1.fk_Mail
RIGHT JOIN t_personnes AS T3 ON T3.id_Personne = T1.fk_Personne


/*
	Permet d'afficher toutes les lignes de la table de droite (t_vehicules) (qui est écrite en sql à droite de t_personnes_vehicules)
	y compris les lignes qui ne sont pas attribuées à des personnes.
*/
SELECT id_Personne, PrenomPersonne, NomPersonne, id_Vehicule, MarqueVehicule, ModeleVehicule  FROM t_personnes_vehicules AS T1
RIGHT JOIN t_vehicules AS T2 ON T2.id_Vehicule = T1.fk_Vehicule
LEFT JOIN t_personnes AS T3 ON T3.id_Personne = T1.fk_Personne


/*
	Affiche TOUS les personnes qui n'ont pas de vehicule attribués
*/
SELECT id_Personne, PrenomPersonne, NomPersonne, id_Vehicule, MarqueVehicule, ModeleVehicule  FROM t_personnes_vehicules AS T1
RIGHT JOIN t_personnes AS T2 ON T2.id_Personne = T1.fk_Personne
LEFT JOIN t_vehicules AS T3 ON T3.id_Vehicule = T1.fk_Vehicule


/*
	Affiche SEULEMENT les personnes qui n'ont pas de vehicule attribués
*/

SELECT id_Personne, PrenomPersonne, NomPersonne, id_Vehicule, MarqueVehicule, ModeleVehicule  FROM t_personnes_vehicules AS T1
RIGHT JOIN t_personnes AS T2 ON T2.id_Personne = T1.fk_Personne
LEFT JOIN t_vehicules AS T3 ON T3.id_Vehicule = T1.fk_Vehicule
WHERE T1.fk_Vehicule IS NULL
