-- Database: DAMATAROMAO_SAMUEL_GESTIONCONSOMMATION_INFO1B_2020

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists DAMATAROMAO_SAMUEL_GESTIONCONSOMMATION_INFO1B_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS DAMATAROMAO_SAMUEL_GESTIONCONSOMMATION_INFO1B_2020;

-- Utilisation de cette base de donnée

USE DAMATAROMAO_SAMUEL_GESTIONCONSOMMATION_INFO1B_2020;

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 17 Juin 2020 à 10:13
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `damataromao_samuel_gestionconsommation_info1b_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_Personne` int(11) NOT NULL,
  `NomPersonne` varchar(42) NOT NULL,
  `PrenomPersonne` varchar(32) NOT NULL,
  `DateNaissancePersonne` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_Personne`, `NomPersonne`, `PrenomPersonne`, `DateNaissancePersonne`) VALUES
(2, 'Jack', 'Daniel', '1866-01-01'),
(3, 'Chivas', 'Regal', '1801-01-01'),
(4, 'Ballantines', 'Blend', '1827-01-01'),
(5, 'Johnnie', 'Walker', '1865-01-01'),
(6, 'Jim', 'Beam', '1795-01-01');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes_vehicules`
--

CREATE TABLE `t_personnes_vehicules` (
  `id_Personne_vehicule` int(11) NOT NULL,
  `FK_Personne` int(11) NOT NULL,
  `FK_Vehicule` int(11) NOT NULL,
  `DateAcquisitionVehicule` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personnes_vehicules`
--

INSERT INTO `t_personnes_vehicules` (`id_Personne_vehicule`, `FK_Personne`, `FK_Vehicule`, `DateAcquisitionVehicule`) VALUES
(1, 2, 1, '2020-03-03 10:23:09'),
(2, 3, 2, '2020-03-03 10:23:22'),
(3, 4, 3, '2020-03-03 10:23:31'),
(6, 5, 5, '2020-06-01 19:03:54'),
(8, 6, 4, '2020-06-02 08:17:02');

-- --------------------------------------------------------

--
-- Structure de la table `t_reservoir`
--

CREATE TABLE `t_reservoir` (
  `id_Reservoir` int(11) NOT NULL,
  `DateRemplissage` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TypeCarburant` varchar(30) NOT NULL,
  `LitresCarburant` tinyint(4) NOT NULL,
  `Kilometre` int(11) NOT NULL,
  `PrixCarburant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_reservoir`
--

INSERT INTO `t_reservoir` (`id_Reservoir`, `DateRemplissage`, `TypeCarburant`, `LitresCarburant`, `Kilometre`, `PrixCarburant`) VALUES
(29, '2020-06-11 07:23:51', 'Essence', 8, 230, 20),
(30, '2020-06-11 07:25:37', 'Essence 98', 3, 23, 5),
(31, '2020-06-11 07:26:24', 'Essence', 30, 786, 40),
(32, '2020-06-11 07:26:48', 'Essence', 12, 60, 23),
(33, '2020-06-11 07:27:22', 'Essence', 40, 980, 55),
(34, '2020-06-11 07:27:46', 'Essence 98', 25, 69, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_vehicules`
--

CREATE TABLE `t_vehicules` (
  `id_Vehicule` int(11) NOT NULL,
  `NomVehicule` varchar(32) NOT NULL,
  `MarqueVehicule` varchar(32) NOT NULL,
  `ModeleVehicule` varchar(32) NOT NULL,
  `AnneeVehicule` smallint(4) DEFAULT NULL,
  `PrixVehicule` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_vehicules`
--

INSERT INTO `t_vehicules` (`id_Vehicule`, `NomVehicule`, `MarqueVehicule`, `ModeleVehicule`, `AnneeVehicule`, `PrixVehicule`) VALUES
(1, 'Mon Audi RS5', 'Audi', 'RS5', 2020, 1039174),
(2, 'Ma Yamaha YZF-R1', 'Yamaha', 'YZF-R1', 2020, 21990),
(3, 'Ma Ford Focus RS', 'Ford', 'Focus RS', 2019, 33350),
(4, 'Ma BMW M2 compétition', 'BMW', 'M2 compétition', 2018, 71340),
(5, 'Ma Mercedes Amg c63', 'Mercedes', 'Amg c63', 2019, 120000);

-- --------------------------------------------------------

--
-- Structure de la table `t_vehicules_reservoir`
--

CREATE TABLE `t_vehicules_reservoir` (
  `id_Vehicule_Reservoir` int(11) NOT NULL,
  `FK_Vehicule` int(11) NOT NULL,
  `FK_Reservoir` int(11) NOT NULL,
  `DateRemplissage` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_vehicules_reservoir`
--

INSERT INTO `t_vehicules_reservoir` (`id_Vehicule_Reservoir`, `FK_Vehicule`, `FK_Reservoir`, `DateRemplissage`) VALUES
(23, 1, 29, '2020-06-11 07:23:51'),
(24, 2, 30, '2020-06-11 07:25:37'),
(25, 3, 31, '2020-06-11 07:26:24'),
(26, 4, 32, '2020-06-11 07:26:48'),
(27, 5, 33, '2020-06-11 07:27:22'),
(28, 1, 34, '2020-06-11 07:27:46');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_Personne`);

--
-- Index pour la table `t_personnes_vehicules`
--
ALTER TABLE `t_personnes_vehicules`
  ADD PRIMARY KEY (`id_Personne_vehicule`),
  ADD KEY `FK_Personne` (`FK_Personne`),
  ADD KEY `FK_Vehicule` (`FK_Vehicule`);

--
-- Index pour la table `t_reservoir`
--
ALTER TABLE `t_reservoir`
  ADD PRIMARY KEY (`id_Reservoir`);

--
-- Index pour la table `t_vehicules`
--
ALTER TABLE `t_vehicules`
  ADD PRIMARY KEY (`id_Vehicule`);

--
-- Index pour la table `t_vehicules_reservoir`
--
ALTER TABLE `t_vehicules_reservoir`
  ADD PRIMARY KEY (`id_Vehicule_Reservoir`),
  ADD KEY `FK_Vehicule` (`FK_Vehicule`),
  ADD KEY `FK_Reservoir` (`FK_Reservoir`),
  ADD KEY `FK_Vehicule_2` (`FK_Vehicule`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `t_personnes_vehicules`
--
ALTER TABLE `t_personnes_vehicules`
  MODIFY `id_Personne_vehicule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_reservoir`
--
ALTER TABLE `t_reservoir`
  MODIFY `id_Reservoir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `t_vehicules`
--
ALTER TABLE `t_vehicules`
  MODIFY `id_Vehicule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_vehicules_reservoir`
--
ALTER TABLE `t_vehicules_reservoir`
  MODIFY `id_Vehicule_Reservoir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_personnes_vehicules`
--
ALTER TABLE `t_personnes_vehicules`
  ADD CONSTRAINT `t_personnes_vehicules_ibfk_1` FOREIGN KEY (`FK_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_personnes_vehicules_ibfk_2` FOREIGN KEY (`FK_Vehicule`) REFERENCES `t_vehicules` (`id_Vehicule`);

--
-- Contraintes pour la table `t_vehicules_reservoir`
--
ALTER TABLE `t_vehicules_reservoir`
  ADD CONSTRAINT `t_vehicules_reservoir_ibfk_1` FOREIGN KEY (`FK_Vehicule`) REFERENCES `t_vehicules` (`id_Vehicule`),
  ADD CONSTRAINT `t_vehicules_reservoir_ibfk_2` FOREIGN KEY (`FK_Reservoir`) REFERENCES `t_reservoir` (`id_Reservoir`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
