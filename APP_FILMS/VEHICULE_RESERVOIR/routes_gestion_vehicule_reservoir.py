# routes_gestion_vehicule_reservoir.py
# Gestions des "routes" FLASK pour la table intermédiaire qui associe les vehicules et les pleins.
from flask import render_template, flash, redirect, url_for, request, session
from APP_FILMS import obj_mon_application
from APP_FILMS.GENRES.data_gestion_genres import GestionVehicule
from APP_FILMS.VEHICULE_RESERVOIR.data_gestion_vehicule_reservoir import GestionVehiculeReservoir
from APP_FILMS.DATABASE.erreurs import *

import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /vehicule_reservoir_afficher_concat
# Récupère la liste de tous les pleins associés a un vehicule présélectionner.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/vehicule_reservoir_afficher_concat/<int:id_film_sel>", methods=['GET', 'POST'])
def vehicule_reservoir_afficher_concat (id_film_sel):
    print("id_vehicule_sel ", id_film_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_genres = GestionVehiculeReservoir()

            # Récupère les données grâce à une requête MySql définie dans la classe GestionGenres()
            valeurs_id_vehicule = {"value_fk_vehicule": id_film_sel}
            # Fichier data_gestion_reservoir.py
            data_vehicule_reservoir_afficher_concat = obj_actions_genres.vehicule_reservoir_afficher_data_concat(id_film_sel)
            dataTPrix = obj_actions_genres.prixtotal(valeurs_id_vehicule)
            dataTKilometre = obj_actions_genres.kilometretotal(valeurs_id_vehicule)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data genres", data_vehicule_reservoir_afficher_concat, "type ", type(data_vehicule_reservoir_afficher_concat))

            print(" data prix total", dataTPrix, "type ",type(dataTPrix))

            print(" data kilomètre total", dataTKilometre, "type ", type(dataTKilometre))

            # Différencier les messages si la table est vide.
            if data_vehicule_reservoir_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données reservoir affichés dans GestionVehiculeReservoir!!", "success")
            else:
                flash(f"""Le plein demandé n'existe pas. Ou la table "t_vehicules_reservoir" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("vehicule_reservoir/vehicule_reservoir_afficher.html",
                           data=data_vehicule_reservoir_afficher_concat, dataTPrix=dataTPrix, dataTKilometre=dataTKilometre)

    # --------------------------------------------------------------------------------------------------------------------------------------------------

@obj_mon_application.route("/vehicule_reservoir_add", methods=['GET', 'POST'])
def vehicule_reservoir_add():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_genres = GestionVehiculeReservoir()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "genres_add.html"
            id_vehicule = request.values['id_film_sel_html']

            type_carburant = request.form['name_typecarb_html']
            litre_carburant = request.form['name_litrecarb_html']
            kilometre = request.form['name_kilometre_html']
            prix_carburant = request.form['name_prixcarb_html']
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.

            if not re.match("[0-9]",
                            litre_carburant):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Entrez correctement le type de carburant, ", "danger")
                # On doit afficher à nouveau le formulaire "genres_add.html" à cause des erreurs de "claviotage"
                return render_template("vehicule_reservoir/vehicule_reservoir_add.html")

            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_type_carburant": type_carburant,
                                                  "value_litre_carburant": litre_carburant,
                                                  "value_kilometre": kilometre,
                                                  "value_prix_carburant": prix_carburant}
                obj_actions_genres.add_reservoir_data(valeurs_insertion_dictionnaire)

                valeurs_id_vehicule = {"value_fk_vehicule": id_vehicule}

                obj_actions_genres.add_vehicule_reservoir_data(valeurs_id_vehicule)



                # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'genres_afficher', car l'utilisateur
                # doit voir le nouveau genre qu'il vient d'insérer. Et on l'affiche de manière
                # à voir le dernier élément inséré.
                return redirect(url_for('vehicule_reservoir_afficher_concat', id_film_sel=id_vehicule))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("vehicule_reservoir/vehicule_reservoir_add.html")

    # --------------------------------------------------------------------------------------------------------------------------------------------------

@obj_mon_application.route('/vehicule_reservoir_select_delete', methods=['POST', 'GET'])
def vehicule_reservoir_select_delete():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_genres = GestionVehiculeReservoir()
            # OM 2019.04.04 Récupère la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_reservoir_delete = request.args.get('id_reservoir_delete')
            fk_vehicule_delete = request.args.get('FK_vehicule_delete')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_reservoir": id_reservoir_delete,
                                          "value_fk_vehicule": fk_vehicule_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_genre = obj_actions_genres.delete_select_reservoir_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('vehicule_reservoir/vehicule_reservoir_delete.html', data=data_id_genre)

    # --------------------------------------------------------------------------------------------------------------------------------------------------

@obj_mon_application.route('/vehicule_reservoir_delete', methods=['POST', 'GET'])
def vehicule_reservoir_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_genres = GestionVehiculeReservoir()
            # OM 2019.04.02 Récupère la valeur de "id_genre" du formulaire html "GenresAfficher.html"
            id_reservoir_delete = request.form['id_reservoir_delete']
            fk_vehicule_delete = request.form['FK_vehicule_delete']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_reservoir": id_reservoir_delete,
                                          "value_fk_vehicule": fk_vehicule_delete,
                                          "value_fk_reservoir": id_reservoir_delete}

            obj_actions_genres.delete_vehicule_reservoir_data(valeur_delete_dictionnaire)
            data_genres = obj_actions_genres.delete_reservoir_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des genres des films
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les genres
            return redirect(url_for('vehicule_reservoir_afficher_concat',id_film_sel=fk_vehicule_delete))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "genre" de films qui est associé dans "t_genres_films".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des films !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce genre est associé à des films dans la t_genres_films !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('vehicule_reservoir_afficher_concat',id_film_sel=fk_vehicule_delete))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('genres/genres_afficher.html', data=data_genres)
