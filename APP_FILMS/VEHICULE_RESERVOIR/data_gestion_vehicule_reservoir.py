# data_gestion_genres_films.py
# OM 2020.04.22 Permet de gérer (CRUD) les données de la table intermédiaire "t_genres_films"

from flask import flash
from APP_FILMS.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.DATABASE.erreurs import *


class GestionVehiculeReservoir():
    def __init__ (self):
        try:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print("dans le try de gestions genres")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion genres films ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionGenresFilms {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionGenresFilms ")

        # --------------------------------------------------------------------------------------------------------------------------------------------------

    def vehicule_reservoir_afficher_data_concat (self, id_film_selected):
        print("id_film_selected  ", id_film_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_vehicule_reservoir_afficher_data_concat = """SELECT * FROM t_vehicules_reservoir AS T1
                                                                INNER JOIN t_vehicules AS T2 ON T2.id_Vehicule = T1.FK_Vehicule
                                                                INNER JOIN t_reservoir AS T3 ON T3.id_Reservoir = T1.FK_Reservoir"""


            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les films
                # Sinon le paramètre représente la valeur de l'id du film
                if id_film_selected == 0:
                    mc_afficher.execute(strsql_vehicule_reservoir_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_film_selected_dictionnaire = {"value_id_film_selected": id_film_selected}
                    strsql_vehicule_reservoir_afficher_data_concat += """ HAVING id_Vehicule= %(value_id_film_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_vehicule_reservoir_afficher_data_concat, valeur_id_film_selected_dictionnaire)

                # Récupère les données de la requête.
                data_vehicule_reservoir_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_vehicule_reservoir_afficher_concat ", data_vehicule_reservoir_afficher_concat, " Type : ",
                      type(data_vehicule_reservoir_afficher_concat))

                # Retourne les données du "SELECT"
                return data_vehicule_reservoir_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

        # --------------------------------------------------------------------------------------------------------------------------------------------------

    def add_reservoir_data (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)


            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_insert_genre_film = """
            INSERT INTO t_reservoir (id_Reservoir, DateRemplissage , TypeCarburant, LitresCarburant , Kilometre, PrixCarburant) VALUES (NULL, CURRENT_TIMESTAMP , %(value_type_carburant)s, %(value_litre_carburant)s, %(value_kilometre)s, %(value_prix_carburant)s);
            
            """

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_genre_film, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # --------------------------------------------------------------------------------------------------------------------------------------------------

    def add_vehicule_reservoir_data (self, valeurs_id_vehicule):
        try:
            print(valeurs_id_vehicule)


            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_insert_genre_film = """
            INSERT INTO `t_vehicules_reservoir` (`id_Vehicule_Reservoir`, `FK_Vehicule`, `FK_Reservoir`, `DateRemplissage`)
            SELECT NULL, %(value_fk_vehicule)s, id_Reservoir, CURRENT_TIMESTAMP FROM t_reservoir ORDER BY id_Reservoir DESC LIMIT 1; 
            """

            #VALUES(NULL, % (value_fk_vehicule)s, (SELECT id_Reservoir FROM Table t_reservoir ORDER BY id_Reservoir DESC), CURRENT_TIMESTAMP);

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_genre_film, valeurs_id_vehicule)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # --------------------------------------------------------------------------------------------------------------------------------------------------

    def prixtotal (self, valeurs_id_vehicule):
        try:
            print(valeurs_id_vehicule)


            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_calc = """
            select SUM(PrixCarburant) as TotalPrix
            from t_reservoir 
            inner join t_vehicules_reservoir on t_reservoir.id_Reservoir = t_vehicules_reservoir.FK_Reservoir
            where t_vehicules_reservoir.FK_Vehicule=%(value_fk_vehicule)s;
            """

            #VALUES(NULL, % (value_fk_vehicule)s, (SELECT id_Reservoir FROM Table t_reservoir ORDER BY id_Reservoir DESC), CURRENT_TIMESTAMP);

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_calc, valeurs_id_vehicule)

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(strsql_calc, valeurs_id_vehicule)
                    dataTPrix = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", dataTPrix)
                    return dataTPrix


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # --------------------------------------------------------------------------------------------------------------------------------------------------

    def kilometretotal (self, valeurs_id_vehicule):
        try:
            print(valeurs_id_vehicule)


            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_calc = """
            select SUM(Kilometre) as TotalKilometre
            from t_reservoir 
            inner join t_vehicules_reservoir on t_reservoir.id_Reservoir = t_vehicules_reservoir.FK_Reservoir
            where t_vehicules_reservoir.FK_Vehicule=%(value_fk_vehicule)s;
            """

            #VALUES(NULL, % (value_fk_vehicule)s, (SELECT id_Reservoir FROM Table t_reservoir ORDER BY id_Reservoir DESC), CURRENT_TIMESTAMP);

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_calc, valeurs_id_vehicule)

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(strsql_calc, valeurs_id_vehicule)
                    dataTKilometre = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", dataTKilometre)
                    return dataTKilometre


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")


    def delete_select_reservoir_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_reservoir = "SELECT t_reservoir.id_Reservoir,t_reservoir.DateRemplissage,t_reservoir.TypeCarburant,t_reservoir.LitresCarburant, t_reservoir.Kilometre, t_reservoir.PrixCarburant , t_vehicules_reservoir.FK_Vehicule FROM t_reservoir " \
                                          "INNER JOIN t_vehicules_reservoir ON t_vehicules_reservoir.FK_Reservoir = t_reservoir.id_Reservoir WHERE id_Reservoir = %(value_id_reservoir)s"

            #SELECT id_Reservoir, DateRemplissage, TypeCarburant, LitresCarburant, Kilometre, PrixCarburant FROM t_reservoir WHERE id_Reservoir = %(value_id_reservoir)s

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_reservoir, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_genre_data Gestions Genres numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_genre_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_genre_data d\'un genre Data Gestions Genres {erreur}")

    # --------------------------------------------------------------------------------------------------------------------------------------------------

    def delete_vehicule_reservoir_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>
            str_sql_delete_reservoir = "DELETE FROM t_vehicules_reservoir WHERE FK_Vehicule = %(value_fk_vehicule)s AND FK_Reservoir = %(value_fk_reservoir)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_reservoir, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_genre_data Data Gestions Genres numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un genre qui est associé à un film dans la table intermédiaire "t_genres_films"
                # il y a une contrainte sur les FK de la table intermédiaire "t_genres_films"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce genre est associé à des films dans la t_genres_films !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !!! Ce genre est associé à des films dans la t_genres_films !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")

    # --------------------------------------------------------------------------------------------------------------------------------------------------




    def delete_reservoir_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>
            str_sql_delete_reservoir = "DELETE FROM t_reservoir WHERE id_Reservoir = %(value_id_reservoir)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_reservoir, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_genre_data Data Gestions Genres numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un genre qui est associé à un film dans la table intermédiaire "t_genres_films"
                # il y a une contrainte sur les FK de la table intermédiaire "t_genres_films"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce genre est associé à des films dans la t_genres_films !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !!! Ce genre est associé à des films dans la t_genres_films !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")

    # --------------------------------------------------------------------------------------------------------------------------------------------------
